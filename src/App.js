import './App.scss';

import { useState } from 'react';

import ThemeContext from './contexts/ThemeContext';
import Header from './components/Header';
import Content from './components/Content';

function App() {
  // const { theme, setTheme } = useContext(ThemeContext);
  const [theme, setTheme] = useState('dark');

  return (
    <ThemeContext.Provider value={{ theme, setTheme }}>
      <div className={`App ${theme}`}>
        <Header />
        <Content />
      </div>
    </ThemeContext.Provider>
  );
}

export default App;
