import './index.scss';

import SearchBar from '../SearchBar';
import Filter from '../Filter';

const Content = () => {
  return (
    <main className="content">
      <div className="input-container">
        <SearchBar />
        <Filter />
      </div>
      <div className="card-list"></div>
    </main>
  );
};

export default Content;
