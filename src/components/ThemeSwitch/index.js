import './index.scss';

import { useContext } from 'react';

import ThemeContext from '../../contexts/ThemeContext';

import { ReactComponent as IconSun } from '../../assets/icon-sun.svg';
import { ReactComponent as IconMoon } from '../../assets/icon-moon.svg';

function ThemeSwitch() {
  const { theme, setTheme } = useContext(ThemeContext);

  const handleThemeSwitchClick = () => {
    setTheme((theme) => (theme === 'dark' ? 'light' : 'dark'));
  };

  return (
    <div className="theme-switch" onClick={handleThemeSwitchClick}>
      <div className="theme-switch__icon">
        {theme === 'dark' ? <IconMoon /> : <IconSun />}
      </div>
      <span className="theme-switch__text">
        {`${theme === 'dark' ? 'Light' : 'Dark'} Mode`}
      </span>
    </div>
  );
}

export default ThemeSwitch;
