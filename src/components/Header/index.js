import './index.scss';

import ThemeSwitch from '../ThemeSwitch';

const Header = () => (
  <header className="header">
    <h1 className="header__text">Where in the world?</h1>
    <ThemeSwitch />
  </header>
);

export default Header;
